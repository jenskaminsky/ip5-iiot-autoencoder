#!/usr/bin/env python
# coding: utf-8

import time
import client
import server
import autoencoder
import data_sensor
import mock_sensor

import matplotlib.pylab as plt


# ------------------------------------------------------------------------------
# main
# ------------------------------------------------------------------------------
data_len = 100 #200
hidden_len = 70 #140
buffer_size = 1000 #2000
epoches = 100
number_of_buffer_pushes = 10 #135

sampled_file_path = r'C:\Users\Jens\Desktop\data_ip5\raw-_sorted-_cleaned_trimmed_resampled_100x.csv'
#sensor = data_sensor.DataSensor(path=sampled_file_path, min_val=0, max_val=14031)
sensor = mock_sensor.MockSensor(signal_to_noise=10, error_rate=0.02, error_duration=10,
                                frequencies=[(1,0.1,0), (0.4,0.005,7), (0.5, 0.03,23)])

client_autoencoder = autoencoder.Autoencoder(data_len, hidden_len)
server_autoencoder = autoencoder.Autoencoder(data_len, hidden_len)
server_autoencoder.setup()

server = server.Server(server_autoencoder, buffer_size, data_len, epoches)
client = client.Client(client_autoencoder, buffer_size, data_len)


for _ in range(2*buffer_size):
    sensor.push_datapoint(client)
dpu = client.get_datapoint_update(remove=True)
server.train_with_new_datapoints(dpu)

start_time = time.time()

for i in range(number_of_buffer_pushes*buffer_size):
    if i%buffer_size == 0:
        dpu = client.get_datapoint_update()
        server.train_with_new_datapoints(dpu)
        server.update_client(client)
        print(i//buffer_size+1, '/', number_of_buffer_pushes)
    sensor.push_datapoint(client)
print('finished')

end_time = time.time()
print('seconds duration:', int(end_time-start_time))

#print('client', sum([client._processing_times1))
#print('server', sum([server._processing_times))

client._display_plot('processing time client', 'iteration', 'time (s)',
                     {'pt1': client._processing_times1,
                      'pt2': client._processing_times2,
                      'pt3': client._processing_times3,
                      'pt4': client._processing_times4})
client._display_plot('processing time server', 'iteration', 'time (s)',
                     {'pt1': server._processing_times1,
                      'pt2': server._processing_times2,
                      'pt3': server._processing_times3,
                      'pt4': server._processing_times4})

best_fit_lag = client.find_lag()

client.print_error_detection_rate(lag=best_fit_lag)
print('best fit lag:', best_fit_lag)

client.plot_datapoints()
client.plot_errors_sigmas()
client.plot_anomaly_detection(lag=best_fit_lag) #best_fit_lag)

sorted_non_zero_errors = sorted([(idx, error) for idx, error in enumerate(client._error_sigma_history) if error != 0], key=lambda x: x[1])
max_idx = sorted_non_zero_errors[-1][0]
min_idx = sorted_non_zero_errors[0][0]

print(max_idx, min_idx)
max_sample = client._datapoint_buffer[max_idx-data_len:max_idx]
min_sample = client._datapoint_buffer[min_idx-data_len:min_idx]

max_pred = client._autoencode(max_sample)
min_pred = client._autoencode(min_sample)

plt.plot(max_sample, label='measured')
plt.plot(max_pred, label='autoencoded')
plt.ylabel('value')
plt.xlabel('datapoint')
plt.title('max error sample')
plt.legend(loc='upper left')
plt.show()

plt.plot(min_sample, label='measured')
plt.plot(min_pred, label='autoencoded')
plt.ylabel('value')
plt.xlabel('datapoint')
plt.title('min error sample')
plt.legend(loc='upper left')
plt.show()
