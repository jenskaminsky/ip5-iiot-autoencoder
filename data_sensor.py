#!/usr/bin/env python
# coding: utf-8

import matplotlib.pyplot as plt
import pandas


class DataSensor:

    def __init__(self, path, min_val, max_val):
        # max_x and min_x are used for normalization
        self._diff = max_val-min_val
        self._min_val = min_val
        self._dataframe = pandas.read_csv(path, sep=',')
        print('dataframe shape:', self._dataframe.shape)
        self._datapoint_generator = (val[1] for val in self._dataframe.values)

    def plot_all_datapoints(self):
        plt.plot([val[1] for val in self._dataframe.values], label='measurements')
        plt.ylabel('value')
        plt.legend(loc='upper left')
        plt.show()

    def push_datapoint(self, client):
        client.evaluate_new_datapoint((self._datapoint_generator.__next__()-self._min_val)/self._diff, False)
