#!/usr/bin/env python
# coding: utf-8

import matplotlib.pyplot as plt
import numpy as np

import time


class Client:

    def __init__(self, autoencoder, buffer_size, data_len, server=None):
        self._autoencoder = autoencoder
        self._buffer_size = buffer_size
        self._data_len = data_len
        self._server = server

        self._datapoint_buffer = []
        self._error_sigma_history = []
        self._known_errors = []
        self._predicted_errors = []
        self._error_mean = 0
        self._error_sigma = 0

        self._processing_times1 = []
        self._processing_times2 = []
        self._processing_times3 = []
        self._processing_times4 = []

    def update_model(self, error_mean, error_sigma):
        self._autoencoder.load()
        self._error_mean = error_mean
        self._error_sigma = error_sigma

    def _calculate_error_sigma(self, x_eval, y_eval):
        error = sum((x - y) ** 2 for x, y in zip(x_eval, y_eval))
        return abs(error - self._error_mean) / self._error_sigma

    def _is_error(self, sigma):
        return sigma > 2

    def _autoencode(self, x_eval):
        return self._autoencoder.predict(np.array([x_eval]))[0]

    def evaluate_new_datapoint(self, datapoint, is_error):
        # performance critical
        ts1 = time.time()

        self._datapoint_buffer.append(datapoint)
        if self._autoencoder._is_trained:
            x_sample = self._datapoint_buffer[-self._data_len:]
            ts2 = time.time()
            y_sample = self._autoencode(x_sample) #improve this
            ts3 = time.time()
            error_sigma = self._calculate_error_sigma(x_sample, y_sample)
        else:
            error_sigma = 0
            ts2 = time.time()
            ts3 = time.time()
        ts4 = time.time()
        self._error_sigma_history.append(error_sigma)
        self._known_errors.append(is_error)
        self._predicted_errors.append(self._is_error(error_sigma))

        ts5 = time.time()
        self._processing_times1.append(ts2 - ts1)
        self._processing_times2.append(ts3 - ts2)
        self._processing_times3.append(ts4 - ts3)
        self._processing_times4.append(ts5 - ts4)

    def get_datapoint_update(self, remove=False):
        if len(self._datapoint_buffer) >= 2 * self._buffer_size:
            update_datapoints = self._datapoint_buffer[:self._buffer_size]
            return update_datapoints
        else:
            return []

    def calc_tfpn(self, expected, predicted):
        tp = 0
        fp = 0
        tn = 0
        fn = 0
        for expc, pred in zip(expected, predicted):
            if expc and pred: tp += 1
            elif expc and not pred: fn += 1
            elif not expc and pred: fp += 1
            else: tn += 1
        return tp,fp,tn,fn

    def find_lag(self):
        min_lag = 0
        max_lag = self._data_len
        shift_predict = self._predicted_errors + [0 for i in range(self._data_len)]
        length = len(self._known_errors)

        shift_val = []
        shift_acc = []

        while max_lag > 1+min_lag:
            lag1 = (max_lag-min_lag)//4+min_lag
            lag2 = (max_lag-min_lag)*3//4+min_lag

            shift_val.extend([lag1, lag2])

            tp1, _, tn1, _ = self.calc_tfpn(self._known_errors, shift_predict[lag1:lag1+length])
            tp2, _, tn2, _ = self.calc_tfpn(self._known_errors, shift_predict[lag2:lag2+length])

            shift_acc.extend([tp1+tn1, tp2+tn2])

            if tp1+tn1 < tp2+tn2:
                min_lag += (max_lag - min_lag) // 2
            else:
                max_lag -= (max_lag-min_lag)//2

        print(max_lag, min_lag)
        plt.scatter(shift_val, shift_acc)
        plt.show()

        return min_lag

    def calc_metrics(self, tp, fp, tn, fn):
        precision = 0
        recall = 0
        f1 = 0
        if tp+fp != 0: precision = 100*tp/(tp+fp)
        if tp+fn != 0: recall = 100*tp/(tp+fn)
        if precision+recall != 0: f1 = 2*precision*recall/recall+precision
        return precision, recall, f1

    def print_error_detection_rate(self, lag=0):

        if lag > 0: shifted_prediction = (self._predicted_errors+[0]*lag)[lag:]
        else: shifted_prediction = self._predicted_errors

        tp, fp, tn, fn = self.calc_tfpn(self._known_errors, shifted_prediction)
        total = sum([tp, fp, tn, fn])
        if total == 0:
            print('no points to compare')
        else:
            print('true_pos:', tp, tp*100/total, '%')
            print('false_pos:', fp, fp*100/total, '%')
            print('true_neg:', tn, tn*100/total, '%')
            print('false_neg:', fn, fn*100/total, '%')

        precision, recall, f1 = self.calc_metrics(tp, fp, tn, fn)
        print('precision:', precision, '%')
        print('recall:', recall, '%')
        print('f1', f1, '%')

    # -------------------------------------------------------------------------
    # plotting
    # -------------------------------------------------------------------------
    def plot_datapoints(self):
        datasets = {'datapoints': self._datapoint_buffer}
        self._display_plot('all datapoints', 'datapoint', 'normalized value', datasets)

    def plot_errors_sigmas(self):
        datasets = {'sigma=3': [3 for _ in range(len(self._error_sigma_history))],
                    'error sigma': self._error_sigma_history}
        self._display_plot('error sigmas', 'datapoint', 'sigma', datasets)

    def plot_anomaly_detection(self, lag=0):
        shifted_anomaly = (self._predicted_errors + [0] * lag)[lag:]
        datasets = {'expected error': self._known_errors,
                    'predicted error': shifted_anomaly}
        self._display_plot('error detection', 'datapoint', 'anomaly=1, normal=0', datasets)

    def plot_most_recent_comparison(self):
        x_sample = self._datapoint_buffer[-self._data_len:]
        datasets = {'measured': x_sample,
                    'autoencoded': self._autoencode(x_sample)}
        self._display_plot('most recent datapoints', 'datapoint', 'value', datasets)

    def _display_plot(self, title, x_label, y_label, datasets):
        for label, dataset in datasets.items():
            plt.plot(dataset, label=label)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.legend(loc='upper left')
        plt.title(title)
        plt.show()
