#!/usr/bin/env python
# coding: utf-8

import random
import numpy as np
import time

import matplotlib.pyplot as plt


class Server:

    def __init__(self, autoencoder, buffer_size, data_len, epoches):
        self._autoencoder = autoencoder
        self._buffer_size = buffer_size
        self._data_len = data_len
        self._epoches = epoches

        self._datapoints = []
        self._mean = 0.0
        self._sigma = 0.0

        self._processing_times1 = []
        self._processing_times2 = []
        self._processing_times3 = []
        self._processing_times4 = []

    def train_with_new_datapoints(self, datapoints):
        ts1 = time.time()

        self._datapoints.extend(datapoints)
        if len(self._datapoints) >= self._buffer_size * 2:
            train_samples = self.get_prioritized_samples(self._buffer_size, self._data_len)
        else:
            train_samples = self.get_random_samples(self._buffer_size, self._data_len)
        ts2 = time.time()

        train_samples = np.array(train_samples)
        ts3 = time.time()

        self._autoencoder.train(train_samples, self._epoches, batch_size=self._buffer_size // 2) #improve
        ts4 = time.time()
        self._autoencoder.save() #improve
        self._calculate_mean_and_sigma() #improve
        ts5 = time.time()
        self._processing_times1.append(ts2 - ts1)
        self._processing_times2.append(ts3 - ts2)
        self._processing_times3.append(ts4 - ts3)
        self._processing_times4.append(ts5 - ts4)

    def update_client(self, client):
        client.update_model(self._mean, self._sigma)

    def _calculate_mean_and_sigma(self):
        no_samples = self._buffer_size
        test_samples = self.get_random_samples(no_samples, self._data_len)
        prediction = self._autoencoder.predict(np.array(test_samples))
        errors = [sum([(test_samples[j][i] - prediction[j][i]) ** 2 for i in range(self._data_len)]) for j in
                  range(len(test_samples))]

        if self._mean == 0.0: self._mean = np.mean(errors)
        else: self._mean = (self._mean + np.mean(errors)) / 2

        if self._sigma == 0.0: self._sigma = np.std(errors)
        else: self._sigma = (self._sigma + np.std(errors)) / 2

        print('mean =', self._mean)
        print('sigma =', self._sigma)

        if False: #plot hist
            plt.hist(errors)
            plt.ylabel('occurences')
            plt.xlabel('error')
            plt.show()

    def _get_sample(self, idx, m):
        return self._datapoints[idx:idx + m]

    def get_random_samples(self, n, m):
        '''
        get random sample from whole pool
        '''
        return [self._get_sample(random.randint(0, len(self._datapoints) - m), m) for _ in range(n)]

    def get_prioritized_samples(self, n, m):
        '''
        get most recent n/2 <results> and from random n/2 <indices>
        '''
        n_new = int(n / 2)
        n_rand = n - n_new
        samples = []
        for idx in range(len(self._datapoints) - n_new - m, len(self._datapoints) - m):
            samples.append(self._get_sample(idx, m))
        for _ in range(n_rand):
            samples.append(self._get_sample(random.randint(0, len(self._datapoints) - m - n), m))
        return samples
