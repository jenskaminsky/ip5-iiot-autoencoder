#!/usr/bin/env python
# coding: utf-8

import math
import random

#make random reproducable
random.seed(1)

class MockSensor:

    def __init__(self, signal_to_noise=1, error_rate=0.0, error_duration=1, frequencies=[(1,0.05,0)]):
        '''
        :param signal_to_noise: ratio between signal and noise aplitude
        :param error_rate: percentage of datapoints that are errors
        :param error_duration: number of consecutive datapoints that are errors (burst length)
        :param frequencies: list of tuples with (amplitude, frequency, phaseshift)
        '''
        self._snr = 0 if signal_to_noise == 0 else 1/signal_to_noise
        self._error_occurrence = error_rate/error_duration
        self._error_duration = error_duration
        self._datapoint_generator = self._generator(frequencies)

    def _generator(self, frequencies):
        i = 0
        sum_aplitudes = sum(freq[0] for freq in frequencies)
        while True:
            i += 1
            rv = sum(math.sin((i+freq[2])*freq[1])*freq[0] for freq in frequencies)/sum_aplitudes
            yield rv

    def push_datapoint(self, client):
        if random.random() < self._error_occurrence:
            for i in range(self._error_duration):
                client.evaluate_new_datapoint(random.random(), True)
        else:
            data_point = max(0, min(1, (self._datapoint_generator.__next__()+1)/2 + self._snr*random.uniform(-1, 1)))
            client.evaluate_new_datapoint(data_point, False)
