#!/usr/bin/env python
# coding: utf-8

from tensorflow.keras.layers import Input, Dense, BatchNormalization, LeakyReLU, Dropout
from tensorflow.keras.models import Model, Sequential, load_model
from tensorflow.keras import regularizers


class Autoencoder:

    def __init__(self, nr_inout_nodes, nr_middle_nodes):
        self._in_layer_size = nr_inout_nodes
        self._h_layer_size = nr_middle_nodes
        self._is_trained = False
        self._model = None

    def setup(self):
        self._model = Sequential()

        self._model.add(Dense(self._in_layer_size, input_shape=(self._in_layer_size,), kernel_regularizer=regularizers.l2(0.001)))
        self._model.add(LeakyReLU(alpha=0.3))

        self._model.add(Dense(self._h_layer_size, kernel_regularizer=regularizers.l2(0.001)))
        self._model.add(LeakyReLU(alpha=0.3))

        self._model.add(Dense(self._in_layer_size))
        self._model.add(LeakyReLU(alpha=0.3))
        self._model.compile(optimizer='adam',
                           loss='mean_squared_error')

    def load(self, file_name = 'autoencoder.h5'):
        self._model = load_model(file_name)
        self._is_trained = True

    def train(self, samples, epochs, batch_size):
        self._model.fit(samples, samples, epochs=epochs, batch_size=batch_size)
        self._is_trained = True

    def predict(self, x):
        return self._model.predict(x)
    
    def save(self, file_name = 'autoencoder.h5'):
        self._model.save(file_name)

